﻿/*
	Original tutorial code by Erik Nordeus <erik.nordeus@gmail.com>
	Modified by Goblinsama Ltd. <goblinsama+src@goblinsama.com>
*/

using UnityEngine;
using System.Collections.Generic;
using Goblinsama;

namespace CatmullRom
{
	[ExecuteInEditMode]
	public class CatmullRomSpline : MonoBehaviour
	{
		// Has to be at least 4 so-called control points
		protected List<WayPoint> controlPointsList = new List<WayPoint>();
		
		[SerializeField] private int steps;
		[SerializeField] private Color32 color;
		
		[SerializeField] protected bool isLooping = true;
		public List<Vector3> wayPoints = new List<Vector3>();
		
		#if UNITY_EDITOR
		[Header("Editor utilities")]
		[SerializeField] private bool UPDATE_POINTS = false;
		#endif
		
		// resolution of the interpolation spline
		private float resolution;
		
		void OnEnable ()
		{
			CheckResolution();
			controlPointsList = new List<WayPoint>(GetComponentsInChildren<WayPoint>());
		}
		
		#if UNITY_EDITOR
		void Update ()
		{
			if (UPDATE_POINTS)
			{
				CheckResolution();
				
				CatMull(true);
				
				if (!isLooping)
				{
					Vector3 tmp = controlPointsList[controlPointsList.Count - 2].transform.position;
					wayPoints.Add(tmp);
				}
				UPDATE_POINTS = false;
			}
		}
		#endif
		
		void OnDrawGizmos ()
		{
			if ( wayPoints.Count < 1 )
				return;
			
			Gizmos.color = color;
			// TODO-. non dovrebbe ricalcolarla ogni volta...
			CatMull(false);
		}
		
		protected virtual void CatMull (bool save)
		{
			if (save)
			{
				wayPoints.Clear();
			}
			
			// Draw the Catmull-Rom lines between the points
			for (int i = 0; i < controlPointsList.Count; i++)
			{
				// Can't draw between the endpoints, neither do we need to draw from the second to the last endpoint... if we are not making a looping line
				if ((i == 0 || i >= controlPointsList.Count-2) && !isLooping)
					continue;
				
				DrawCatmullRomSpline(i,save);
			}
		}
		
		// check the resolutions of the spline
		private void CheckResolution ()
		{
			if (steps <= 0)
				return;
			
			resolution = 1f / (float)steps;
			
			if (resolution < 0f || resolution > 1f)
			{
				Debug.LogWarning(this+" wrong resolution ["+resolution+"].");
				resolution = 0.1f;
			}
		}
		
		// Returns a position between 4 Vector3 with Catmull-Rom Spline algorithm see http://www.iquilezles.org/www/articles/minispline/minispline.htm
		private Vector3 ReturnCatmullRom (float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
		{
			Vector3 a = 0.5f * (2f * p1);
			Vector3 b = 0.5f * (p2 - p0);
			Vector3 c = 0.5f * (2f * p0 - 5f * p1 + 4f * p2 - p3);
			Vector3 d = 0.5f * (-p0 + 3f * p1 - 3f * p2 + p3);
			
			Vector3 pos = a + (b * t) + (c * t * t) + (d * t * t * t);
			
			return pos;
		}
		
		// Display a spline between 2 points derived with the Catmull-Rom spline algorithm
		private void DrawCatmullRomSpline (int pos, bool save)
		{
			// Clamp to allow looping
			Vector3 p0 = controlPointsList[ClampListPos(pos - 1)].transform.position;
			Vector3 p1 = controlPointsList[pos].transform.position;
			Vector3 p2 = controlPointsList[ClampListPos(pos + 1)].transform.position;
			Vector3 p3 = controlPointsList[ClampListPos(pos + 2)].transform.position;
			
			// Just assign a tmp value to this
			Vector3 lastPos = Vector3.zero;
			
			// t is always between 0 and 1 and determines the resolution of the spline 0 is always at p1
			for (float t = 0; t < 1; t += resolution)
			{
				// Find the coordinates between the control points with a Catmull-Rom spline
				Vector3 newPos = ReturnCatmullRom(t, p0, p1, p2, p3);
				
				if (save)
				{
					wayPoints.Add(newPos);
				}
				else
				{
					// Can't display anything the first iteration
					if (t == 0)
					{
						lastPos = newPos;
						continue;
					}
					
					Gizmos.DrawLine(lastPos, newPos);
					lastPos = newPos;
				}
			}
			
			if (!save)
			{
				// Also draw the last line since it is always less than 1, so we will always miss it
				Gizmos.DrawLine(lastPos, p2);
			}
		}
		
		// Clamp the list positions to allow looping start over again when reaching the end or beginning
		private int ClampListPos (int pos)
		{
			if (pos < 0)
			{
				pos = controlPointsList.Count - 1;
			}
			
			if (pos > controlPointsList.Count)
			{
				pos = 1;
			}
			else if (pos > controlPointsList.Count - 1)
			{
				pos = 0;
			}
			
			return pos;
		}
	}
}
