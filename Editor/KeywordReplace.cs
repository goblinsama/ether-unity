/*
	Originally created by hpjohn
	http://forum.unity3d.com/threads/c-script-template-how-to-make-custom-changes.273191/#post-1806467
*/

using UnityEngine;
using UnityEditor;
using System.Collections;
 
public class KeywordReplace : UnityEditor.AssetModificationProcessor
{
	public static void OnWillCreateAsset ( string path )
	{
		path = path.Replace( ".meta", "" );
		
		int index = path.LastIndexOf( "." );
		if (index<0)
			return;
		
		string file = path.Substring( index );
		if ( file != ".cs" && file != ".js" && file != ".boo" ) return;
		index = Application.dataPath.LastIndexOf( "Assets" );
		path = Application.dataPath.Substring( 0, index ) + path;
		file = System.IO.File.ReadAllText( path );
		
		string namespaceName = PlayerSettings.productName;
		namespaceName = namespaceName.Replace(' ','_');
		
		file = file.Replace( "#CREATIONDATE#", System.DateTime.Now + "" );
		file = file.Replace( "#PROJECTNAME#", namespaceName );
		file = file.Replace( "#DEVELOPERS#", PlayerSettings.companyName );
		
		System.IO.File.WriteAllText( path, file );
		AssetDatabase.Refresh();
	}
}
