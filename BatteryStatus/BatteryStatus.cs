/*
	© 2016 Goblinsama Ltd., All rights reserved
*/

using UnityEngine;

namespace BatteryTools
{
	public class BatteryStatus : MonoBehaviour
	{
		// http://forum.unity3d.com/threads/battery-status-level.268024/#post-2505536
		// by alok_androider, 2016 Feb 11
		public static float GetBatteryLevel ()
		{
			#if UNITY_IOS
			
			throw new System.NotImplementedException("BatteryStatus bugged for <"+Application.platform+">.");
			
			/*
			UIDevice device = UIDevice.CurrentDevice();
			device.batteryMonitoringEnabled = true; // need to enable this first
			Debug.Log("Battery state: " + device.batteryState);
			Debug.Log("Battery level: " + device.batteryLevel);
			return device.batteryLevel*100;
			*/
			
			#elif UNITY_ANDROID
			
			if (Application.platform == RuntimePlatform.Android)
			{
				try
				{
					using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
					{
						if (null != unityPlayer)
						{
							using (AndroidJavaObject currActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
							{
								if (null != currActivity)
								{
									using (AndroidJavaObject intentFilter = new AndroidJavaObject("android.content.IntentFilter", new object[]{ "android.intent.action.BATTERY_CHANGED" }))
									{
										using (AndroidJavaObject batteryIntent = currActivity.Call<AndroidJavaObject>("registerReceiver", new object[]{null,intentFilter}))
										{
											int level = batteryIntent.Call<int>("getIntExtra", new object[]{"level",-1});
											int scale = batteryIntent.Call<int>("getIntExtra", new object[]{"scale",-1});
											
											// Error checking that probably isn't needed but I added just in case.
											if (level == -1 || scale == -1)
											{
												return 50f;
											}
											return ((float)level / (float)scale) * 100.0f;
										}
									}
								}
							}
						}
					}
				}
				catch (System.Exception)
				{
					
				}
			}
			
			return 100;
			
			#else
			
			throw new System.NotImplementedException("BatteryStatus not implemented for <"+Application.platform+">.");
			
			#endif
		}
	}
}
