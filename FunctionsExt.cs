/*
	Functions in this file mostly come from public sources, as linked.
	Modified by Goblinsama, as needed.
*/

using UnityEngine;
using System.Text.RegularExpressions;

namespace Ether
{
	public static class FunctionsExt
	{
		private static System.DateTime _epoch = System.DateTime.MinValue;
		public static System.DateTime epoch {
			get {
				if (_epoch == System.DateTime.MinValue)
					_epoch = new System.DateTime(1970, 1, 1, 0, 0, 0);
				return _epoch;
			}
		}
		
		public static int GetEpochDays ()
		{
			return Mathf.FloorToInt(Timestamp()/(3600*24)); // così segna solo i giorni
		}
		
		// http://stackoverflow.com/a/17632585/207655
		public static long Timestamp ()
		{
			return (long)( System.DateTime.UtcNow.Subtract(epoch) ).TotalSeconds;
		}
		
		public static System.DateTime EpochDaysToDateTime (int days)
		{
			return epoch.AddDays(days);
		}
		
		// http://answers.unity3d.com/answers/637243/view.html
		public static void LookAt2D (Transform source, Vector3 target)
		{
			Vector3 diff = target - source.position;
			diff.Normalize();
			
			float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
			source.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
		}
		
		// by Shadeless, http://forum.unity3d.com/threads/efficient-tile-map-for-platformer.270211/#post-1786807
		public static float RoundToNearestPixel (float unityUnits, float pixelToUnits)
		{
			float valueInPixels = unityUnits * pixelToUnits;
			valueInPixels = Mathf.Round(valueInPixels);
			float roundedUnityUnits = valueInPixels * (1 / pixelToUnits);
			return roundedUnityUnits;
		}
		public static Vector3 RoundToNearestPixel (Vector3 source, float pixelToUnits)
		{
			Vector3 ret;
			ret.x = RoundToNearestPixel(source.x,pixelToUnits);
			ret.y = RoundToNearestPixel(source.y,pixelToUnits);
			ret.z = RoundToNearestPixel(source.z,pixelToUnits);
			return ret;
		}
		
		public static string RemoveSpecialCharacters ( string str )
		{
			string re = "[^a-zA-Z0-9_.]+";
			string sub = "";
			
			#if UNITY_STANDALONE
			return Regex.Replace( str, re, sub, RegexOptions.Compiled );
			#else
			return Regex.Replace( str, re, sub );
			#endif
		}
	}
}
