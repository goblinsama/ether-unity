﻿/*
	From Unity Standard Assets
	Modified by Goblinsama Ltd.
*/

using UnityEngine;
using Goblinsama;

namespace UnityStandardAssets.Utility
{
	public class SmoothFollow : MonoBehaviourE
	{
		// The target we are following
		[SerializeField] protected Transform target;
		
		/// <summary>
		/// If true, it will use relative position to target as pivot.
		/// If false, it will use provided distance and height.
		/// </summary>
		[SerializeField] private bool startPositionRelative;
		
		// The distance in the x-z plane to the target
		[SerializeField] private float distance = 10.0f;
		
		// the height we want the camera to be above the target
		[SerializeField] private float height = 5.0f;
		
		[SerializeField] private float rotationDamping;
		[SerializeField] private float heightDamping;
		
		protected override void Start ()
		{
			base.Start();
			
			if (target && startPositionRelative)
			{
				LockTarget(target);
			}
		}
		protected override bool LateUpdate ()
		{
			if ( base.LateUpdate() )
				return true;
			
			// Early out if we don't have a target
			if (!target)
				return true;
			
			// Calculate the current rotation angles
			float wantedRotationAngle = target.eulerAngles.y;
			float wantedHeight = target.position.y + height;
			
			float currentRotationAngle = transform.eulerAngles.y;
			float currentHeight = transform.position.y;
			
			// Damp the rotation around the y-axis
			currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
			
			// Damp the height
			currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);
			
			// Convert the angle into a rotation
			Quaternion currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
			
			// Set the position of the camera on the x-z plane to:
			// distance meters behind the target
			transform.position = target.position;
			transform.position -= currentRotation * Vector3.forward * distance;
			
			// Set the height of the camera
			transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);
			
			// Always look at the target
			transform.LookAt(target);
			
			return false;
		}
		
		/// <summary>
		/// Sets a new target, using position relative to that target as the new pivot.
		/// (this means the Camera will stay still, and only move when the new target moves)
		/// </summary>
		public void LockTarget ( Transform newTarget )
		{
			target = newTarget;
			
			Vector3 test = newTarget.position;
			test.y = transform.position.y;
			
			distance = Vector3.Distance(transform.position, test);
			height = transform.position.y - newTarget.position.y;
		}
		
		/// <summary>
		/// Sets a new target, keeping current pivot.
		/// (this means the Camera will move towards the new target)
		/// </summary>
		public void ChangeTarget ( Transform newTarget )
		{
			target = newTarget;
		}
	}
}
